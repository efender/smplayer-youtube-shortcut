Components.utils.import("resource://gre/modules/Services.jsm");

var EXPORTED_SYMBOLS = ["prefByOS"];

var prefByOS = {

    /**
    * Changes the default process filepath preference depending on the operating system.
    * Linux: "/usr/bin/smplayer"
    * Windows 32-bit: "C:\Program Files\SMPlayer\smplayer.exe"
    * Windows 64-bit: "C:\Program Files (x86)\SMPlayer\smplayer.exe"
    * This function only takes effect once, and only if the user has not changed the preferences.
    */
    change: function() {
        if(!used){
            var prefsBranch = Components.classes["@mozilla.org/preferences-service;1"]
                .getService(Components.interfaces.nsIPrefService)
                .getBranch("extensions.smplayer_shortcut.");
            var hasUserValue = prefsBranch.prefHasUserValue("smplayer_filepath");
            var prefValue = prefsBranch.getCharPref("smplayer_filepath");
            if(!hasUserValue || prefValue === ""){
                var OS = Services.appinfo.OS;
                var filePath = "";
                if(OS == "Linux"){
                    filePath = "/usr/bin/smplayer";
                }
                else if(OS == "WINNT"){

                    var file = Components.classes["@mozilla.org/file/local;1"]
                            .createInstance(Components.interfaces.nsILocalFile);
                    //trying path for 32-bit Windows
                    file.initWithPath("C:\\Program Files\\SMPlayer\\smplayer.exe");
                    if(file.exists()){
                        filePath = "C:\\Program Files\\SMPlayer\\smplayer.exe";
                    }
                    //trying path for 64-bit Windows
                    else {
                        file.initWithPath("C:\\Program Files (x86)\\SMPlayer\\smplayer.exe");
                        if(file.exists()){
                            filePath = "C:\\Program Files (x86)\\SMPlayer\\smplayer.exe";
                        }
                    }
                }
                prefsBranch.setCharPref("smplayer_filepath", filePath);
            }
            used = true;
        }
    }

}

var used = false;
