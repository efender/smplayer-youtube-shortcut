Components.utils.import("chrome://smplayer_shortcut/content/smplayer_module.jsm");

var smplayerMenuItem = {

    /**
    * Used as an Event Listener for the window element on "load".
    * Adds the showhide function to the contextMenu event listeners.
    */
    init : function() {
        var contextMenu = document.getElementById("contentAreaContextMenu");
        if (contextMenu) contextMenu.addEventListener("popupshowing", smplayerMenuItem.showHide, false);
        prefByOS.change();
    },

    /**
    * Used as an Event Listener for the context menu on "popupshowing".
    * Initialises the "hidden" state of the context-menu buttons
    * depending on the location of the context-menu click.
    */
    showHide : function(event) {
        var smplayer_play = document.getElementById("smplayer_play");
        var smplayer_enqueue = document.getElementById("smplayer_enqueue");
        var smplayer_playLink = document.getElementById("smplayer_play_link");
        var smplayer_enqueueLink = document.getElementById("smplayer_enqueue_link");
        var youtubeURL = false;
        if (gContextMenu.linkURL) {
            youtubeURL = ytVidId(gContextMenu.linkURL);
            if (youtubeURL === false) {
                smplayer_playLink.hidden = true;
                smplayer_enqueueLink.hidden = true;
            }
            else {
                smplayer_playLink.hidden = false;
                smplayer_enqueueLink.hidden = false;
            }
        }
        else
            {
                smplayer_playLink.hidden = true;
                smplayer_enqueueLink.hidden = true;
            }
        youtubeURL = ytVidId(gBrowser.contentDocument.location.href);
        if (youtubeURL === false) {
            smplayer_play.hidden = true;
            smplayer_enqueue.hidden = true;
        }
        else {
            smplayer_play.hidden = false;
            smplayer_enqueue.hidden = false;
        }
    }
}

var smplayerProcess = {

    /**
    * Initialises the process to call.
    * Adds the command path to the process
    */
    initProcess : function(){
        smplayerProcess.process = Components.classes["@mozilla.org/process/util;1"]
            .createInstance(Components.interfaces.nsIProcess);
        smplayerProcess.file = Components.classes["@mozilla.org/file/local;1"]
            .createInstance(Components.interfaces.nsILocalFile);
        var prefs = Components.classes["@mozilla.org/preferences-service;1"]
            .getService(Components.interfaces.nsIPrefService)
            .getBranch("extensions.smplayer_shortcut.");
        var path = prefs.getCharPref("smplayer_filepath");
        smplayerProcess.file.initWithPath(path);
        smplayerProcess.process.init(smplayerProcess.file);


    },

    playVideo : function(url) {
        smplayerProcess.initProcess();
        var args = [];
        args[0] = "-minigui";
        //Getting rid of the https part of the URL, because VLC doesn't process it well.
        //Thanks to the Mozilla user 'lunchboffin'
        if(url.substring(0, 5) == 'https'){
            url = 'http' + url.substring(5);
        }
        args[1] = url;
        try{
            smplayerProcess.process.run(false, args, args.length);
        } catch(err) {
            var errorMsg = "Error: " + err.message;
            alert(errorMsg);

        }

    },

    enqueueVideo : function(url) {
        smplayerProcess.initProcess();
        var args = [];
        args[0] = "-minigui";
        args[1] = "-add-to-playlist";
        //Getting rid of the https part of the URL, because VLC doesn't process it well.
        //Thanks to the Mozilla user 'lunchboffin'
        if(url.substring(0, 5) == 'https'){
            url = 'http' + url.substring(5);
        }
        args[2] = url;
        smplayerProcess.process.run(false, args, args.length);
        return false;

    }


};

/**
* Function for browsing the filepath of the process to call
* when the smplayerProcess.playVideo and smplayerProcess.enqueueVideo are called
* The filepath must point to a command or an executable file (depending on the Operating System).
*/
function browseFilePath(){

    const nsIFilePicker = Components.interfaces.nsIFilePicker;
    const nsILocalFile = Components.interfaces.nsILocalFile;
    var fp = Components.classes["@mozilla.org/filepicker;1"]
               .createInstance(nsIFilePicker);
    fp.init(window, "Browse", nsIFilePicker.modeOpen);
    fp.appendFilters(nsIFilePicker.filterAll);
    var retVal = fp.show();
    if (retVal == nsIFilePicker.returnOK ) {
        var pickedFile = fp.file.QueryInterface(nsILocalFile);
        if (pickedFile.isExecutable() && pickedFile.isFile()) {
            var prefsFilePicker = Components.classes["@mozilla.org/preferences-service;1"]
            .getService(Components.interfaces.nsIPrefService)
            .getBranch("extensions.smplayer_shortcut.");
            prefsFilePicker.setComplexValue("smplayer_filepath", nsILocalFile, pickedFile);
        }
        else {

            alert(document.getElementById("smplayer_shortcut_stringBundle").getString("error.executableFile"));
        }
    }

}




/**
 * JavaScript function to match (and return) the video Id
 * of any valid Youtube Url, given as input string.
 * @author: Stephan Schmitz <eyecatchup@gmail.com>
 * @url: http://stackoverflow.com/a/10315969/624466
 */
function ytVidId(url) {
  var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
  return (url.match(p)) ? RegExp.$1 : false;
}

window.addEventListener("load",smplayerMenuItem.init,false);


//window.addEventListener("load",prefByOS.change,false);
