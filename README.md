"SMPlayer Youtube Shortcut" - the extension for Firefox
===

---

**SMPlayer Youtube Shortcut** is a fork of "[VLC Youtube Shortcut](https://addons.mozilla.org/ru/firefox/addon/vlc-youtube-shortcut/)"

**SMPlayer Youtube Shortcut** and "[YouTube Embed2Link](https://addons.mozilla.org/en-US/firefox/addon/youtube-embed2link/)" - a perfect combination


